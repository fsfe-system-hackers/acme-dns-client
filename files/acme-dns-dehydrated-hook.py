#!/usr/bin/env python3

"""Dehydrated hook"""

import sys
import json
import requests

ACME_CONFIG = "/etc/acmedns/clientstorage.json"

# Operation chosen by dehydrated
operation = sys.argv[1]


def open_acme_client(acme_config):
    """Open JSON file with acme-dns client data"""
    try:
        with open(acme_config) as f:
            output = json.load(f)
            return output

    except FileNotFoundError:
        print(
            f"{acme_config} missing. You probably have to register with the "
            "acme-dns client first."
        )
        sys.exit(1)


def read_acme_client(data):
    """Read acme-dns client data"""
    try:
        return (
            data[domain]["fulldomain"],
            data[domain]["subdomain"],
            data[domain]["username"],
            data[domain]["password"],
            data[domain]["server_url"],
        )

    except KeyError:
        print(
            f"No ACME DNS data found for {domain}. Please register it with the "
            "acme-dns server using the acme-dns-client."
        )
        sys.exit(1)


if operation == "deploy_challenge":
    domain, txt = sys.argv[2], sys.argv[4]

    # Open JSON file with acme-dns client data
    d = open_acme_client(ACME_CONFIG)

    # Read acme-dns client data
    fulldomain, subdomain, username, password, server_url = read_acme_client(d)

    # Make request to acme-dns API
    headers = {"X-Api-User": username, "X-Api-Key": password}
    payload = {"subdomain": subdomain, "txt": txt}

    r = requests.post(f"{server_url}/update", headers=headers, data=json.dumps(payload))
    if r.status_code == 200:
        print("Update successful")
    else:
        print(
            f"The TXT record update failed. This is what I've tried:\n"
            f"{fulldomain}\tIN\tTXT\t{txt}\n\n"
            f"That's the return error: {r.json()}"
        )
        sys.exit(1)
