---
- name: Convert space-separated domain list to actual list
  set_fact:
    domains_list: "{{ item.split(' ') }}"
  with_items: "{{ domains }}"
  when: "' ' in domains[0]"

- name: Domain list seems to be valid, set unchanged fact therefore
  set_fact:
    domains_list: "{{ domains }}"
  with_items: "{{ domains }}"
  when: "' ' not in domains[0]"

- name: Install required packages (Python 3)
  apt:
    name:
      - python3-pexpect
      - python3-requests
  when: ansible_python.version.major==3

- name: Install required packages (Python 2)
  apt:
    name:
      - python-pexpect
      - python-requests
  when: ansible_python.version.major==2

- name: Deploy acme-dns-client
  copy:
    src: acme-dns-client-v0.2
    dest: /usr/local/bin/acme-dns-client
    owner: root
    group: root
    mode: 0755

- name: Deploy acme-dns dehydrated hook
  copy:
    src: acme-dns-dehydrated-hook.py
    dest: /usr/local/bin/acme-dns-dehydrated-hook
    owner: root
    group: root
    mode: 0755

- name: Register domains with acme-dns server
  expect:
    command: acme-dns-client register -d "{{ item }}" -s "https://{{ acme_dns_server }}"
    responses:
      CAA record: n
      CNAME record change: n
  loop: "{{ domains_list }}"

- name: Scan locally installed packages
  package_facts:
    manager: "auto"
  delegate_to: localhost

- name: Check whether jmespath is available
  fail:
    msg: "No jmespath python module available on your machine (localhost). This is required for local JSON queries."
  when:
    - "'python-jmespath' not in ansible_facts.packages"
    - "'python3-jmespath' not in ansible_facts.packages"

- name: Check whether dnspython is available
  fail:
    msg: "No dnspython python module available on your machine (localhost). This is required for local DNS queries."
  when:
    - "'python-dnspython' not in ansible_facts.packages"
    - "'python3-dnspython' not in ansible_facts.packages"

- name: Check if ACME CNAME record is correct
  include_tasks: acme_subdomain_check.yml
  loop: "{{ domains_list }}"
