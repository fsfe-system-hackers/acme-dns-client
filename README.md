# ACME DNS Client Role

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/acme-dns-client/00_README)

This role can be used in playbooks to install the client part of the [acme-dns
server](https://git.fsfe.org/fsfe-system-hackers/acme-dns/).

## What is does

1. Deploys the acme-dns-client (for registration with server and certbot) and
   the dehydrated hook.
2. Runs acme-dns-client for the given domains.
3. Checks whether the CNAME record has been set. Only when this is done, you
   should continue with getting a cert with certbot/dehydrated.

## Variables

* `domains` is a list of domains which shall be registered.
* `acme_dns_server` is the acme-dns server. This defaults to the FSFE's
  instance.
